# datasource-json

    json datasource for AWS AppSync

## Installation

    git clone http://bitbucket.org/mswinson-lib/datasource-json.git


## Prequisites

    docker
    docker-compose

## Usage

setup  

    export S3_BUCKET_NAME=<mybucket>
    export S3_BUCKET_PREFIX=<mybucket_prefix>
    export AWS_DEFAULT_REGION=<aws_region>

build  

    docker-compose run build

    > make build


deploy

    docker-compose run build

    > make deploy
  

## Contributing

