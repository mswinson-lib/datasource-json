const getJSON = require('./utils/get_json.js');

//lambda handler
//@param event [Object] options
//@param context [Object] context object
//@param callback [Function] callback function
//@return None
exports.handler = async function(event, context, callback) {
  var url = event.url;

  if (url !== null) {
    var json = await getJSON(url);

    callback(null, json);
    return;
  } else {
    callback(null, {});
    return;
  }
}
