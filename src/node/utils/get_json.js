const https = require('https');

// validate status code
// @param statusCode [Int] status code
// @return None
// @raise RequestFailed - if status is not 200
var valid_status = function(statusCode) {
  let error;
  if (statusCode !== 200) {
     error = new Error('Request Failed.\n' +
                       `Status Code: ${statusCode}`);

    throw error;
  }
}

// validate content type
// @param contentType [String] string to test
// @return None
// @raise InvalidContentType - if contenttype is not valid
var valid_contentType = function(contentType) {
  let error;
  switch(contentType) {
    case (contentType.match(/^application\/json/) || {}).input:
    case (contentType.match(/^text\//) || {}).input:
      break;
    default:
      error = new Error('Invalid content-type.\n' + contentType);
      throw error;
  }
}

// getJSON
// fetch json
// @param url [String] url
// @return [Object]
var getJSON = function(url) {
	return new Promise((resolve,reject) => {
    https.get(url, (res) => {
      try {
        const { statusCode } = res;
        const contentType = res.headers['content-type'];

        valid_status(statusCode);
        valid_contentType(contentType);

        res.setEncoding('utf8');
        let rawData = '';
        res.on('data', (chunk) => { rawData += chunk; });
        res.on('end', () => {
          const parsedData = JSON.parse(rawData);
          resolve(parsedData);
        });
      } catch(error) {
        res.resume();
        reject(error);
      }
    });
  });
}

module.exports = exports = getJSON;
